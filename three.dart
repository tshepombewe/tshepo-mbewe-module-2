class App{
  String name = "";
  String category ="";
  String developer ="";
  String year = "";
  
  App( this.name, this.category , this.developer, this.year);

  void transform_caps(){
    print(name.toUpperCase());
  }
}

void main(){
  var app = new App("Sumsokol","finance","banavision","2022");
  print("name : ${app.name}");
  print("category : ${app.category}");
  print("developer : ${app.developer}");
  print("year : ${app.year}");
  print("\n");
  app.transform_caps();
}